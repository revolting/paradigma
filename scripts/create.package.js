var fs = require('fs');

var packageJson = fs.createReadStream('./package.json', {flags: 'r', encoding: 'utf-8'});

packageJson.on('data', function (d) {
    if (process.argv.length == 3) {
        var pk = JSON.parse(d.toString());
        // posicion de la version
        pk.version = process.argv[2];
        fs.writeFile("./package.json", JSON.stringify(pk));
    }
    packageJson.destroy();
});