const path = require('path');
const webpack = require('webpack');
const htmlWebpackPlugin = require('html-webpack-plugin');
const autoprefixer = require('autoprefixer');
const extractTextPlugin = require('extract-text-webpack-plugin');

/////////////////// CONFIG ///////////////////
const packageJson = require('./../package.json');
const host = 'localhost';
const port = 3000;
const mockPort = 3002;
const dirname = path.join(__dirname, '../');
///////////////// END CONFIG /////////////////

module.exports = {
    devtool: 'cheap-module-eval-source-map',
    entry: {// Punto de entrada de la aplicacion
        index: [
            `webpack-dev-server/client?http://${host}:${port}/`,
            path.join(dirname, 'client/main.js')
        ]
    },
    output: {
        path: path.join(dirname, 'dist'), //Ruta de la salida de la compilacion de webpack
        filename: '[name].js', //Nombre del fichero de la salida
        chunkFilename: '[chunkhash][id][name].js',
        publicPath: `http://localhost:3000/`
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('development'),
            },
            'ENVIRONMENT': JSON.stringify('DEV')
        }),
        new webpack.NamedModulesPlugin(),
        new webpack.optimize.ModuleConcatenationPlugin(),
        new htmlWebpackPlugin({
            template: path.join(dirname, 'staticDist', 'index.html'),
            path: path.join(dirname, 'dist'),
            filename: 'index.html',
            inject: false
        }),
        new extractTextPlugin('[name].css')
    ],
    resolve: {
        alias: {
            "fbjs/lib/setImmediate": "fbjs/lib/setimmediate"
        },
        extensions: ['.js'],
        modules: ['./', 'node_modules', './client']
    },
    module: {
        rules: [
            {
                enforce: "pre",
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "eslint-loader",
                options: {
                    emitError: true,
                    emitWarning: true,
                    failOnError: true
                }
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    query: {
                        babelrc: false,
                        presets: [
                            ['es2015', {modules: false}],
                            'react',
                            'stage-0'
                        ],
                        plugins: [
                            'transform-decorators-legacy'
                        ],
                    }
                }
            },
            {
                test: /\.(xlsx|pdf|xml|docx|csv)$/,
                use: 'file-loader?name=[path][name].[ext]'
            },
            {
                test: /\.(png|gif|jpg|svg)$/,
                use: 'file-loader?name=[path][hash].[ext]'
            },
            {
                test: /\.css$/,
                use: extractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                importLoaders: 1
                            }
                        },
                        'postcss-loader'
                    ]
                })
            },
            {
                test: /\.less$/,
                use: extractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                importLoaders: 1
                            }
                        },
                        'postcss-loader',
                        {
                            loader: 'less-loader'
                        }
                    ]
                })
            },
            {
                test: /\.woff2(\?\S*)?$/,
                use: 'url-loader?limit=100000&mimetype=application/font-woff'
            },
            {
                test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
                use: 'url-loader?limit=100000&mimetype=application/font-woff'
            },
            {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                use: 'file-loader?name=[path][name].[ext]'
            },
            {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                use: 'file-loader?name=[path][name].[ext]'
            },
            {
                test: /\.json$/,
                exclude: /node_modules/,
                use: 'json-loader'
            }
        ]
    }
};
