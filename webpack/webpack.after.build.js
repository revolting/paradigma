const fs = require('fs');
const path = require('path');

/////////////////// CONFIG ///////////////////
const dirname = path.join(__dirname, '../');
const distDirname = path.join(__dirname, '../', 'dist');
///////////////// END CONFIG /////////////////

var source, target;

var packageJson = fs.createReadStream(path.join(dirname, 'package.json'), {flags: 'r', encoding: 'utf-8'});

// Copia del fichero de configuracion de sonar a partir de la version de package.json
var version = "";
packageJson.on('data', function (d) {
    var pk = JSON.parse(d.toString());

    //Creacion del nuevo package json para publicar
    var newPackage = {
        "name": pk.name,
        "version": pk.version,
        "description": pk.description,
        "main": "index.js",
        "publishConfig": pk.publishConfig
    };

    fs.writeFileSync(path.join(distDirname, 'package.json'), JSON.stringify(newPackage));

    packageJson.destroy();
});



//Copia del fichero de readme
source = fs.createReadStream(path.join(dirname, 'readme.md'));
target = fs.createWriteStream(path.join(distDirname, 'readme.md'));
source.pipe(target);

//Copia del fichero de .npmignore
source = fs.createReadStream(path.join(dirname, '.npmignore'));
target = fs.createWriteStream(path.join(distDirname, '.npmignore'));
source.pipe(target);
