const path = require('path');
const webpack = require('webpack');
const cleanWebpackPlugin = require('clean-webpack-plugin');
const autoprefixer = require('autoprefixer');
const extractTextPlugin = require('extract-text-webpack-plugin');
const htmlWebpackPlugin = require('html-webpack-plugin');

/////////////////// CONFIG ///////////////////
const packageJson = require('./../package.json');
const dirname = path.join(__dirname, '../');
///////////////// END CONFIG /////////////////


module.exports = {
    devtool: 'source-map',
    entry: {
        index: [
            path.join(dirname, 'client/main.js')
        ]
    },
    output: {
        path: path.join(dirname, 'dist'), //Ruta de la salida de la compilacion de webpack
        filename: '[name].js', //Nombre del fichero de la salida
        chunkFilename: '[chunkhash][id][name].js',
        publicPath: `/`
    },
    plugins: [
        new cleanWebpackPlugin(['dist'], {
            root: path.join(dirname),
            verbose: true,
            dry: false
        }),
        new webpack.LoaderOptionsPlugin({
            minimize: true,
            debug: false
        }),
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production'),
            },
            'ENVIRONMENT': JSON.stringify('PRE')
        }),
        new htmlWebpackPlugin({
            template: path.join(dirname, 'staticDist', 'index.html'),
            path: path.join(dirname, 'dist'),
            filename: 'index.html',
            inject: false
        }),
        new webpack.optimize.ModuleConcatenationPlugin(),
        new webpack.NamedModulesPlugin(),
        new extractTextPlugin('[name].css')
    ],
    resolve: {
        alias: {
            "fbjs/lib/setImmediate": "fbjs/lib/setimmediate"
        },
        extensions: ['.js'],
        modules: ['./', 'node_modules', './client']
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    query: {
                        babelrc: false,
                        presets: [
                            ['es2015', {modules: false}],
                            'react',
                            'stage-0'
                        ],
                        plugins: [
                            'transform-decorators-legacy'
                        ],
                    }
                }
            },
            {
                test: /\.(xlsx|pdf|xml|docx|csv)$/,
                use: 'file-loader?name=[path][name].[ext]'
            },
            {
                test: /\.(png|gif|jpg|svg)$/,
                use: 'file-loader?name=[path][hash].[ext]'
            },
            {
                test: /\.css$/,
                use: extractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                importLoaders: 1
                            }
                        },
                        'postcss-loader'
                    ]
                })
            },
            {
                test: /\.less$/,
                use: extractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                importLoaders: 1
                            }
                        },
                        'postcss-loader',
                        {
                            loader: 'less-loader'
                        }
                    ]
                })
            },
            {
                test: /\.woff2(\?\S*)?$/,
                use: 'url-loader?limit=100000&mimetype=application/font-woff'
            },
            {
                test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
                use: 'url-loader?limit=100000&mimetype=application/font-woff'
            },
            {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                use: 'file-loader?name=[path][hash].[ext]'
            },
            {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                use: 'file-loader?name=[path][hash].[ext]'
            },
            {
                test: /\.json$/,
                exclude: /node_modules/,
                use: 'json-loader'
            }
        ]
    }
};