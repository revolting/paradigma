/**
 * Created by javier.laguna on 13/06/2017.
 */
window.matchMedia = window.matchMedia || function() {
        return {
            matches : false,
            addListener : function() {},
            removeListener: function() {}
        };
    };