import {combineReducers} from 'redux-immutable';
import appReducer from '05-reducers/app/appReducer';
import localeReducer from '05-reducers/locale/localeReducer';
import phonesReducer from '05-reducers/phones/phonesReducer';

export default combineReducers({
    app: appReducer,
    locale: localeReducer,
    phones: phonesReducer
})