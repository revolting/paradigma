// Utils
import {switchReducer, builderCases} from '05-reducers/utils';
// State
import initialState from '05-reducers/00-initialState/app/appInitialState';

const cases = builderCases({
});

export default function appReducer(state = initialState, action) {
    return switchReducer(state, cases, action);
}