import {fromJS} from 'immutable';

export default fromJS({
    displayDetail: false, // Indica si ha de mostrarse los detalles en caso de que se haya seleccionado un movil
    phones: [], // Conjunto de moviles
    selectedPhone:{
        brand: '-',
        camera: '-',
        dimensions: '-',
        image: '',
        name: '-',
        processor: '-',
        weight: '-'
    }
});