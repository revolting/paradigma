// Utils
import {switchReducer, builderCases} from '05-reducers/utils';

// Constants
import * as types from 'constants/phonesConstants';

// State
import initialState from '05-reducers/00-initialState/phones/phonesInitialState';

const cases = builderCases({
    [types.DISPLAY_DETAILS]: (state, action) => {
        return state.set('displayDetail', action.display)
            .set('selectedPhone', action.selectedPhone);
    }
});

export default function phonesReducer(state = initialState, action) {
    return switchReducer(state, cases, action);
}