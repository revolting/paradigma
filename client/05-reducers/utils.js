export function switchReducer(state, cases, action) {
    return (cases[action.type] || cases.default)(state, action);
}

export function builderCases(cases) {
    if (!cases.default) {
        cases['default'] = (state => (state));
    }
    return cases;
}