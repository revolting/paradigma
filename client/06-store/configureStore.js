import {compose, createStore, applyMiddleware} from 'redux';
import reducers from '05-reducers/index';
import thunk from 'redux-thunk';

const middleware = [thunk];

export default function configureStore(initialState) {
    const createStoreWithMiddleware = compose(
        applyMiddleware(...middleware)
    )(createStore);
    return createStoreWithMiddleware(reducers, initialState);
}
