import esES from 'i18n/languages/es-ES.json';
import enUS from 'i18n/languages/en-US.json';

export function getLocale(locale = 'enUS') {
    locale = locale.replace('_','');
    locale = locale.replace('-','');
    switch (locale) {
        case 'enUS':
            return enUS;
        case 'esES':
            return esES;
        default:
            return enUS;
    }
}