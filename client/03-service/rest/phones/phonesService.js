import {executeServiceCall} from '03-service/rest/delegate';
import * as paths from 'constants/service/rest';
import _util from 'lodash/util';

export function getPhones(callback = _util.noop, error = _util.noop) {
    executeServiceCall(paths.PHONES, 'GET').then(callback).catch(error);
}