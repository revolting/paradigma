import axios from 'axios';

export function executeServiceCall(actionURL, httpMethod = 'POST') {
    return axios({
        method: httpMethod,
        url: actionURL,
        validateStatus: function (status) {
            return status >= 200 && status < 300; // default
        }
    }).then(function (response) {
        return response.data;
    });
}