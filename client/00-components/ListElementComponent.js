// Libraries & Utils
import React, {Component} from 'react';
import PropTypes from 'prop-types'

export default class ListElementComponent extends Component {
    static propTypes = {
        image: PropTypes.string, // Imagen
        name: PropTypes.string, // Nombre
        onClick: PropTypes.func // Muestra los detalles del movil
    };

    // Muestra los detalles del movil seleccionado
    onClick = ()=>{
        this.props.onClick(this.props.name);
    };

    render() {
        return (
            <article className='phone-container__list__element'>
                <section className='phone-container__list__element__mobile'>
                    <img alt='phone' src={location.origin+'/'+this.props.image} onClick={this.onClick}/>
                </section>
            </article>
        )
    }
}