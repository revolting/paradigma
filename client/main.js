// Libraries & Utils
import 'utils/polyfill';
import {fromJS} from 'immutable';

// Services
import * as services from '03-service/rest/phones/phonesService';

// Locale
import {getLocale} from 'i18n';

// Components & Containers
import router from './router';
import loading from 'loading';

// Styles
import 'normalize.css';
import 'assets/styles/index.less';

// Initial states
import appInitialState from '05-reducers/00-initialState/app/appInitialState';
import localeInitialState from '05-reducers/00-initialState/locale/localeInitialState';
import phonesInitialState from '05-reducers/00-initialState/phones/phonesInitialState';

// Locale
let locale = getLocale();

// Loading layer
let loadingDiv = document.createElement('div');
// Si se cambia el id del preloading hay que cambiarlo dentro del componente
loadingDiv.id = 'loading';
document.body.appendChild(loadingDiv);

// App layer
let appDiv = document.createElement('div');
appDiv.id = 'app';

document.body.appendChild(appDiv);

// Mostramos el loading
loading();

// Seteamos los estados iniciales
// eslint-disable-next-line
function initState(locale, phones) {
    // Locale del usuario
    let localeIS = localeInitialState;
    localeIS = localeIS.set('words', locale);

    // Locale del usuario
    let phonesIS = phonesInitialState;
    phonesIS = phonesIS.set('phones', phones);

    return fromJS({
        app: appInitialState,
        locale: localeIS,
        phones: phonesIS
    })
}

// Hacemos la llamada a Back para recibir la lista de moviles a mostrar
services.getPhones((phones) => {
    // Hacemos un setTimeOut para la version dummy, simulando que estamos cargando datos,
    // ya que tarda muy poco la peticion  
    setTimeout(() => {
        loading(false);
        locale = getLocale('esES');
        router(initState(fromJS(locale), fromJS(phones)));
    }, 2000);
});