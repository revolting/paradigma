// Libraries & Utils
import React, {PureComponent} from 'react';
import {connect} from 'react-redux';

// Containers
import Header from '01-containers/app/HeaderContainer';
import PhoneContainer from '01-containers/phones/PhoneContainer';

function mapStateToProps() {
    return {}
}

function mapDispatchToProps() {
    return {};
}

@connect(mapStateToProps, mapDispatchToProps())
export default class AppContainer extends PureComponent {
    render() {
        return (
            <div className='app-content'>
                <Header/>
                <section className='page-container'>
                    <PhoneContainer/>
                </section>
            </div>
        )
    }
}