// Libraries & Utils
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types'
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';

// Assets
import logoPhonetastic from 'assets/images/logo2.svg';
const logo = logoPhonetastic;

function mapStateToProps(state) {
    return {
        labels: state.getIn(['locale', 'words']) // Conjunto de literales que contiene la aplicacion
    }
}

function mapDispatchToProps() {
    return {};
}

@withRouter
@connect(mapStateToProps, mapDispatchToProps())
export default class HeaderContainer extends PureComponent {
    static propTypes = {
        labels: PropTypes.object
    };

    // Traduccion de literales
    translateSrv = (tag)=>{
        return this.props.labels.get(tag) || tag;
    };

    render() {
        return (
            <header className='app-header'>
                <section className='app-header__element app-header__element__logo'>
                    <img alt='logoApp' src={logo}/>
                </section>
                <section className='app-header__element app-header__element__menu'>
                    <nav className='app-header__element__menu_option'>{this.translateSrv('HOME')}</nav>
                    <nav className='app-header__element__menu_option'>{this.translateSrv('ABOUT_US')}</nav>
                    <nav className='app-header__element__menu_option'>{this.translateSrv('CONTACT')}</nav>
                </section>
            </header>
        )
    }
}