// Libraries & Utils
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types'
import {connect} from 'react-redux';
import classnames from 'classnames';

function mapStateToProps(state) {
    return {
        labels: state.getIn(['locale', 'words']) // Conjunto de literales que contiene la aplicacion
    }
}

function mapDispatchToProps() {
    return {};
}

@connect(mapStateToProps, mapDispatchToProps())
export default class PhoneDetailComponent extends PureComponent {
    static propTypes = {
        brand: PropTypes.string, // Marca
        camera: PropTypes.string, // Camara
        dimensions: PropTypes.string, // Dimensiones
        get: PropTypes.func,
        hideDetails: PropTypes.func,
        image: PropTypes.any,
        labels: PropTypes.object,
        name: PropTypes.string, // Nombre
        processor: PropTypes.string, // Procesador
        weight: PropTypes.string // Peso
    };

    state = {
        displayDetails: false, // Indica si han de mostrarse las caracteristicas del movil
        hideDetails: false // Indica si han de ocultarse los datos (fadeout)
    };

    componentDidMount(){
        setTimeout(() => {
            this.setState({
                displayDetails: true // iniciamos la animacion fadein
            });
        }, 500);
    }

    // Traduccion de literales
    translateSrv = (tag)=>{
        return this.props.labels.get(tag) || tag;
    };

    // Ocultamos los detalles del movil
    hideDetails = ()=>{
        this.setState({
            displayDetails: false, // Ocultamos el contenedor de los datos
            hideDetails: true // Animacion fadeout
        });

        setTimeout(() => {
            this.props.hideDetails();
        }, 250);
    };

    render() {
        let infoStyles = classnames({
            'phone-container__info__content': true,
            'phone-container__info__content_display': this.state.displayDetails,
            'phone-container__info__content_hide': this.state.hideDetails
        });

        return (
            <article className={infoStyles}>
                <section className='phone-container__info__content__mobile'>
                    <img alt='phone' src={location.origin+'/'+this.props.image}/>
                </section>
                <section className='phone-container__info__content__description'>
                    <header className='phone-container__info__content__description__title'>
                        {this.props.name}
                        <i className='far fa-window-close' onClick={this.hideDetails}/>
                    </header>
                    <section className='phone-container__info__content__description__content'>
                        <p className='phone-container__info__content__description__content__row'>
                            <span className='phone-container__info__content__description__content__row_field'>{this.translateSrv('BRAND')}:</span>
                            <span className='phone-container__info__content__description__content__row_value'>{this.props.brand}</span>
                        </p>
                        <p className='phone-container__info__content__description__content__row'>
                            <span className='phone-container__info__content__description__content__row_field'>{this.translateSrv('DIMENSIONS')}:</span>
                            <span className='phone-container__info__content__description__content__row_value'>{this.props.dimensions}</span>
                        </p>
                        <p className='phone-container__info__content__description__content__row'>
                            <span className='phone-container__info__content__description__content__row_field'>{this.translateSrv('WEIGHT')}:</span>
                            <span className='phone-container__info__content__description__content__row_value'>{this.props.weight}</span>
                        </p>
                        <p className='phone-container__info__content__description__content__row'>
                            <span className='phone-container__info__content__description__content__row_field'>{this.translateSrv('PROCESSOR')}:</span>
                            <span className='phone-container__info__content__description__content__row_value'>{this.props.processor}</span>
                        </p>
                        <p className='phone-container__info__content__description__content__row'>
                            <span className='phone-container__info__content__description__content__row_field'>{this.translateSrv('CAMERA')}:</span>
                            <span className='phone-container__info__content__description__content__row_value'>{this.props.camera}</span>
                        </p>
                    </section>
                </section>
            </article>
        )
    }
}