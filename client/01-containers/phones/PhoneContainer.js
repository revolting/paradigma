// Libraries & Utils
import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {connect} from 'react-redux';
import classnames from 'classnames';

// Actions
import {displayDetails} from '02-actions/phonesActions';

// Components
import PhoneDetailComponent from '01-containers/phones/PhoneDetailComponent';
import ListElementComponent from '00-components/ListElementComponent';

function mapStateToProps(state) {
    return {
        displayDetail: state.getIn(['phones', 'displayDetail']), // True si ha de mostrarse los detalles del movil
        phones: state.getIn(['phones', 'phones']), // Conjunto de moviles
        selectedPhone:  state.getIn(['phones', 'selectedPhone']) // Movil seleccionado
    }
}

function mapDispatchToProps() {
    return {displayDetails};
}

@connect(mapStateToProps, mapDispatchToProps())
export default class PhoneContainer extends Component {
    static propTypes = {
        displayDetail: PropTypes.bool,
        displayDetails: PropTypes.func,
        map: PropTypes.func,
        phones: PropTypes.object,
        selectedPhone: PropTypes.object
    };

    // Mostramos los datos del movil
    displayDetails = (name)=>{
        let selectedPhone = this.props.phones.find((phone)=>{return phone.get('name') === name});
        this.props.displayDetails(true, selectedPhone);
    };

    // Ocultamos los datos del movil
    hideDetails = ()=>{
        this.props.displayDetails(false);
    };

    render() {
        let infoStyles = classnames({
            'phone-container__info': true,
            'phone-container__info_expand': this.props.displayDetail
        });
        return (
            <section className='phone-container'>
                <section className='phone-container__carousel'/>
                <section className={infoStyles}>
                    {this.props.displayDetail?
                        <PhoneDetailComponent
                            brand={this.props.selectedPhone.get('brand')}
                            camera={this.props.selectedPhone.get('camera')}
                            dimensions={this.props.selectedPhone.get('dimensions')}
                            image={this.props.selectedPhone.get('image')}
                            hideDetails={this.hideDetails}
                            name={this.props.selectedPhone.get('name')}
                            processor={this.props.selectedPhone.get('processor')}
                            weight={this.props.selectedPhone.get('weight')}
                        />:
                        null}
                </section>
                <section className='phone-container__list'>
                    {
                        this.props.phones.map((phone, i)=>{
                            return <ListElementComponent
                                image={phone.get('image')}
                                key={i}
                                name={phone.get('name')}
                                onClick={this.displayDetails}/>
                        })
                    }
                </section>
            </section>
        )
    }
}