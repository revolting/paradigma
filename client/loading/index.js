import React from 'react';
import ReactDOM from 'react-dom';
import 'client/assets/styles/core/app/loader.less';

export default function Loading(show = true) {
    ReactDOM.render(
        <div className={show?'LoaderBalls':'LoaderBalls_hide'}>
            <div className='LoaderBalls__item'/>
            <div className='LoaderBalls__item'/>
            <div className='LoaderBalls__item'/>
        </div>
        , document.getElementById('loading'));
}