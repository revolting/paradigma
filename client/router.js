import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import Loadable from 'react-loadable';
import configureStore from '06-store/configureStore';
import {HashRouter, Route} from 'react-router-dom';
import App from '01-containers/app/AppContainer';

//Containers para el router
export default ((initialStateStore) => {
    const store = configureStore(initialStateStore);

    function loading() {
        return <div>Loading...</div>
    }

    render(
        <Provider store={store}>
            <HashRouter>
                <App>
                    <Route path='/'
                           component={Loadable({
                               loader: () => import('01-containers/phones/PhoneContainer'),
                               loading
                           })}
                    />
                </App>
            </HashRouter>
        </Provider>
        , document.getElementById('app')
    );
})
