// Constants
import * as types from 'constants/phonesConstants';

// Muestra las caracteristicas de un movil
export function displayDetails(display, selectedPhone){
    return {
        type: types.DISPLAY_DETAILS,
        display,
        selectedPhone
    }
}