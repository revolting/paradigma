// Server
const express = require("express");
const app = express();

// Webpack
const webpack = require('webpack');
const webpackMiddleware = require('webpack-dev-middleware');
const webpackConfig = require('../webpack/webpack.config.pro.js');

// Data
const phoneList = [
    {
        brand: 'Samsung',
        camera: '12 MP, AF Dual Pixel, OIS, apertura variable f/1.5-2.4. Vídeo cámara súper lenta 960fps con detección de movimiento',
        dimensions: '147,7 x 68,7 x 8,5 mm',
        image: 'static/s9.png',
        name: 'Galaxy S9',
        processor: 'Samsung Exynos 9810, 10 nm, 64 bits, octa-core',
        weight: '163 g'
    },
    {
        brand: 'Apple',
        camera: 'Cámara Dual 12 MP (Apertura f/1.8 + f/2.8). Estabilización óptica de imagen (OIS). Fotos panorámicas (hasta 63 Mpx)',
        dimensions: '143,6 x 70,9 x 7,7 mm',
        image: 'static/iphonex.png',
        name: 'iPhone X',
        processor: 'Apple A11 Bionic',
        weight: '174 g'
    },
    {
        brand: 'Xiaomi',
        camera: '20+12MP, f/1.75, 1.25µm, PDAF, vídeo 4K/30fps',
        dimensions: '158,7 x 75,4 x 7,3 mm',
        image: 'static/mia2.png',
        name: 'Mi A2',
        processor: 'Snapdragon 660',
        weight: '168 gramos'
    },
    {
        brand: 'Huawei',
        camera: '16 megapíxeles f/2.2, enfoque PDAF',
        dimensions: '148,6 x 71,2 x 7,4 mm',
        image: 'static/p20lite.png',
        name: 'P20 Lite',
        processor: 'Kirin 659 a 2,36GHz GPU Mali-T830 MP2',
        weight: '146 gramos'
    },
    {
        brand: 'Samsung',
        camera: '12 megapíxeles con una lente con OIS y f/1,7',
        dimensions: '148,9 x 68,1 x 8 mm',
        image: 'static/s8.png',
        name: 'Galaxy S8',
        processor: 'Exynos 8895 o Qualcomm Snapdragon 835 ',
        weight: '155 gramos'
    },
    {
        brand: 'Xiaomi',
        camera: '12 mpx',
        dimensions: '155,5 x 75,2 x 8,8 mm',
        image: 'static/pocophone.png',
        name: 'Pocophone F1',
        processor: 'Qualcomm Snapdragon 845 2.8 GHz',
        weight: '182 g'
    }
];

// Middleware necesario para la comunicacion entre Express y webpack (Coge
// la aplicacion Front y la sirve en la ruta inicial)
app.use(webpackMiddleware(webpack(webpackConfig)));

// Para servir las imagenes en ruta relativa
app.use('/static', express.static(__dirname + '/public'));

// Punto de entrada de la aplicacion
app.get('/phones', function (req, res) {
    res.json(phoneList);
});

// Puerto por donde la aplicacion escucha las peticiones del cliente
app.listen(3000, function () {
    console.log('Servidor escuchando por el puerto 3000!');
});