# Phonetastic

Phonetastic is an application developed by Manuel Rebollo for listing mobile phones with their associated features.

## Installation

### Requirements
* node

## Usage

`$ npm i`
`$ node ./server/server`

```python
http://localhost:3000/index.html
```